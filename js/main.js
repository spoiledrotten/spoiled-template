$(document).ready(() => {
  console.log("ready");

  let toggle = $("#toggler");
  let hiddenmenu = $("#shiftedmenu");
  let storedItem = localStorage.getItem("openmenu");

  if (storedItem === "1") {
    hiddenmenu.addClass("show");
  } else {
    hiddenmenu.removeClass("show");
  }

  if (hiddenmenu.hasClass("show")) {
    localStorage.setItem("openmenu", "1");
    hiddenmenu.addClass("show");
  } else {
    localStorage.removeItem("openmenu");
    hiddenmenu.removeClass("show");
  }

  toggle.click(() => {
    hiddenmenu.toggleClass("show");
    if (hiddenmenu.hasClass("show")) {
      localStorage.setItem("openmenu", "1");
      hiddenmenu.addClass("show");
    } else {
      localStorage.removeItem("openmenu");
      hiddenmenu.removeClass("show");
    }
  });
});

(function($) {
  $(document).ready(function() {
    $("[title]").style_my_tooltips({
      tip_follows_cursor: true,
      tip_delay_time: 000,
      tip_fade_speed: 200
    });
  });
})(jQuery);
